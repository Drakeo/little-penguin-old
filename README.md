Little Penguin  Viewer
====================
This project merges with the source code for the
[Second Life](https://www.secondlife.com) Viewer.

This source is available as open source; for details on licensing, see
# README #
# CEF Media Plugin  With Dullahan Host Linux viewer. 
Liden Labs no longer supports the linux viewer so I have done my best to keep up.
This is a copy of the linux Little Penquin viewer is a fork of 
https://bitbucket.org/Drakeo/viewer/src/master/ .
dedicated for Linux users
# Release for Slackware64 14.2 and later.
# Compiled  Slackware64 14.2  with GCC 5.5.0 GLIBC 2.23
# Compiled  Slackware64 15.0  with GCC 10.3.0 GLIBC 2.33

enjoy. 
CEF branch 4103.  Chromium: 83.0.4103.97
#  KDE5 Chromium and Kwallet asking for password.
 KDE5 create a another wallet name it. Suggestion "littlepenguin" when promted click blowfish. when asked for password do not type one in.
 It will then complain are you sure and click yes. now set that wallet to default. 
  [More info on Kwallet](https://utils.kde.org/projects/kwalletmanager/)
 
[the licensing page on the Second Life wiki](https://wiki.secondlife.com/wiki/Linden_Lab_Official:Second_Life_Viewer_Licensing_Program)

For information on how to use and contribute to this, see
[the open source portal on the wiki](https://wiki.secondlife.com/wiki/Open_Source_Portal).

To download the current default version, visit
[the download page](https://secondlife.com/support/downloads). For
even newer versions try
[the Alternate Viewers page](https://wiki.secondlife.com/wiki/Linden_Lab_Official:Alternate_Viewers)
  

